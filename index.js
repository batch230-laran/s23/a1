// console.log("Hello World!");




// ------------------------------ 3 - 5. 



let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
}


trainer.talk();
console.log(trainer);





// ------------------------------ 6. 



console.log("Result of dot notation: ");
console.log(trainer["name"]);


console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);


console.log("Result of talk method: ");
trainer.talk();






// ------------------------------ 7. 




function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level*2;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		target.health -= this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health)
	}

	this.faint = function() {
		console.log(this.name + " fainted");
	}
}



let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);


geodude.tackle(pikachu);

console.log(pikachu);


mewtwo.tackle(geodude);

geodude.faint();


console.log(geodude);











